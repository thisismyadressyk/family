// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import iView from 'iview'
import 'iview/dist/styles/iview.css'
import axios from 'axios' //引入axios
import 'babel-polyfill'
import moment from "moment";
Vue.prototype.$moment = moment;

Vue.config.productionTip = false;
Vue.use(iView);

const G=window.g;
let url=G.url;
//axios.defaults.baseURL = process.env.API_HOST;
axios.defaults.baseURL =url;
Vue.prototype.$http= axios;



router.beforeEach((to, from, next) =>{
  if (to.meta.requireAuth) {  // 判断该路由是否需要登录权限
  if (sessionStorage.getItem('token')) {  // 通过sessionStorage获取当前的token是否存在
      next();
    }
    else {
      next({
        path: '/',
        query: {redirect: to.fullPath}  // 将跳转的路由path作为参数，登录成功后跳转到该路由
      })
    }
  }
  else {
    next();
  }
})



/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
