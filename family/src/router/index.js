import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Login from '@/components/login'
import Level from '@/components/child/level'
import Gift from '@/components/child/gift'
import Exchange from '@/components/child/exchange'
import Record from '@/components/child/record'
import UserManage from '@/components/child/userManage'
import Rules from '@/components/child/rules'




Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'login',
      component: Login,
    },
    {
      path: '/record',
      name: 'HelloWorld',
      component: HelloWorld,
      children:[
        {
          path:'/level',
          name:'Level',
          component:Level,
          meta: {
            requireAuth: true,  // 添加该字段，表示进入这个路由是需要登录的
          },
        },
        {
          path:'/gift',
          name:'Gift',
          component:Gift,
          meta: {
            requireAuth: true,  // 添加该字段，表示进入这个路由是需要登录的
          },
        },
        {
          path:'/record',
          name:'Record',
          component:Record,
          meta: {
            requireAuth: true,  // 添加该字段，表示进入这个路由是需要登录的
          },
        },
        {
          path:'/exchange',
          name:'Exchange',
          component:Exchange,
          meta: {
            requireAuth: true,  // 添加该字段，表示进入这个路由是需要登录的
          },
        },
        {
          path:'/userManage',
          name:'UserManage',
          component:UserManage,
          meta: {
            requireAuth: true,  // 添加该字段，表示进入这个路由是需要登录的
          },
        },
        {
          path:'/rules',
          name:'Rules',
          component:Rules,
          meta: {
            requireAuth: true,  // 添加该字段，表示进入这个路由是需要登录的
          },
        },
      ],
    }

  ]
})
