'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',//测试
  //API_HOST:'"http://47.106.218.124:20022/checkIn"'
})
